let number = 1;


var slider = document.body.querySelector('.slider');
slider.scrollLeft = 391;

//Function working simmilarly to "map()" in p5.js. yoinked from user mx_code on stack overflow.
const scale = (numberr, fromRange, toRange) => {
    return (
      ((numberr - fromRange[0]) * (toRange[1] - toRange[0])) /
        (fromRange[1] - fromRange[0]) +
      toRange[0]
    );
};


slider.addEventListener('scroll', function() {
    var scrollPoss = slider.scrollLeft;
    //Maps the value from scroll position form a value betwen 0,800 to a corresponding value from 0 to 60.
    var scrollPosFix = scale(scrollPoss, [0,800], [0,60]);
    var postButton = document.querySelector('.makeAPostButton');
    //When on roughly center section show the "add post" button.
    if(scrollPosFix < 30 && scrollPosFix > 28) {
    postButton.style.visibility = 'visible';
    } else {
    postButton.style.visibility = 'hidden';
    };
    var opaScaler = scale(scrollPoss, [0,800], [0,2]);
    console.log(opaScaler);
    var dots = document.querySelectorAll('.dot')
    
    //If statement for left most dot opacity
    if (opaScaler < 1) {
        dots[0].style.opacity = 1.3-opaScaler;
    } else {
        dots[0].style.opacity = 0.3;
    }

    //If statement for right most dot opacity
    if (opaScaler > 1) {
        dots[2].style.opacity = -0.7+opaScaler;
    } else {
        dots[2].style.opacity = 0.3;
    }

    //calculating the opaity of the center dot based on the opacity of the dots beside it.
    dots[1].style.opacity = 1.9 - dots[0].style.opacity - dots[2].style.opacity
    console.log(dots[2].style.opacity)

    var distFromCenter = 1.5-opaScaler

});


//Function for swiping horizontally between pages
/*
const init = function(){
    let items = document.querySelectorAll('section');
    for (let i = 0; i < items.length; i++){
      items[i].style.background = randomColor({luminosity: 'light'});
    }
    cssScrollSnapPolyfill()
  }
  init();
*/
class Post {
    constructor(iD, postTitle, chapter, page, assignment, picture, description, comments, bell, heart) {
        this.iD = iD;
        this.postTitle = postTitle;
        this.chapter = chapter;
        this.page = page;
        this.assignment = assignment;
        this.picture = picture;
        this.description = description;
        this.comments = comments;
        this.bell = bell;
        this.heart = heart;
    }
};

function postPost() {
    // Title definition
    let postTitleform = document.getElementById('postTitleForm');
    let postTitle = postTitleform.value;
    //console.log("the Title is " + postTitle);
    // Chapter definition
    let postChapterform = document.getElementById('chapterNr');
    let chapter = postChapterform.value;
    //console.log("the chapter is " + chapter);
    // Page definition
    let postPageform = document.getElementById('pageNr');
    let page = postPageform.value;
    //console.log("the page is " + page);
    // Assignment definiiton
    let postAssignmentform = document.getElementById('assignmentNr');
    let assignment = postAssignmentform.value;
    //console.log("the asignment number is " + assignment);
    // Question description definition
    let postDescriptionform = document.getElementById('descriptionForm');
    let description = postDescriptionform.value;
    //console.log("the description is " + description);
    //Make the post
    let heart = 0;
    let comments = 0;
    let bell = 0;
    let iD = 1;
    var addedPhoto = document.createElement("addedPhoto");
    addedPhoto.src = "Assets/hypotenusen.png";
    let picture = addedPhoto;

    //Create a class containing all info on the post allowing for methods such as turning bell for a specific post on and off 
    //and potentially making a method for a drop down menu comment section.
    let post1 = new Post(iD, postTitle, chapter, page, assignment, picture, description, comments, bell, heart);
    
    //Adds all the information from the form in a new dynamically created div.
    const newDiv = document.createElement("div");
    newDiv.setAttribute('id','Post');
    
    //Add new div for post title and give it an id.
    const newPostTitleDiv = document.createElement("div");
    newPostTitleDiv.setAttribute('id','PostTitle');

    //Add new div for chapter/page/assignment (CPA) and give it an id.
    const newCPADiv = document.createElement("div");
    newCPADiv.setAttribute('id', 'CPA');

    //Add new div for the text problembeskrivelse
    const problemDiv = document.createElement("div");
    problemDiv.setAttribute('id', 'problemDescription')

    //Add new div for post description and give it an id.
    const newPostDescriptionDiv = document.createElement("div");
    newPostDescriptionDiv.setAttribute('id', 'PostDescription');

    //Add new div to contain bottom action bar for posts
    const actionBarDiv = document.createElement("div");
    actionBarDiv.setAttribute('id', 'actionBar');

    //Creates a divs for the images on the action bar and gives them all id's relating to which number post they are.
    const bellDiv = document.createElement("div");
    bellDiv.setAttribute('class', 'bell');
    bellDiv.setAttribute('id', number);
    

    const heartDiv = document.createElement("div");
    heartDiv.setAttribute('class', 'heart');
    heartDiv.setAttribute('id', number);

    const arrowDiv = document.createElement("div");
    arrowDiv.setAttribute('class', 'arrow');
    arrowDiv.setAttribute('id', number);


    //Creates text nodes to fill in the post with.
    const newPostTitle = document.createTextNode(post1.postTitle);
    const newPostChapter = document.createTextNode("Kapitel: " + post1.chapter +", ");
    const newPostPage = document.createTextNode("Side: " + post1.page +", ");
    const newPostAssignment = document.createTextNode("Opgave: " + post1.assignment);
    const problemDescription = document.createTextNode("Problembeskrivelse:")
    const newPostDescription = document.createTextNode(post1.description);

    //Appends Title div to post div, and adds text into Title div
    newDiv.appendChild(newPostTitleDiv);
    newPostTitleDiv.appendChild(newPostTitle);

    //Appends CPA div to newDiv (newDiv = the div for a new post) 
    //and then appends all the text info from the form relating to chapter, page, assignment number.
    newDiv.appendChild(newCPADiv);
    newCPADiv.appendChild(newPostChapter);
    newCPADiv.appendChild(newPostPage);
    newCPADiv.appendChild(newPostAssignment);

    //Appends post description to post description div
    newDiv.appendChild(problemDiv);
    problemDiv.appendChild(problemDescription)
    newDiv.appendChild(newPostDescriptionDiv);
    newPostDescriptionDiv.appendChild(newPostDescription);

    //Appends the action bar div to the post div
    newDiv.appendChild(actionBarDiv);
    
    //loads images and adds them to the action bar.

    //Append container for bell image
    actionBarDiv.appendChild(bellDiv);
    
    //Load inactive bell image
    var bellOff = document.createElement('img');
    bellOff.src = "Assets/bellOff.svg";
    bellDiv.appendChild(bellOff);
    bellOff.setAttribute('onclick', "bellChange(this)");
    bellOff.setAttribute('class', 'bellOff');

    //Append container for heart image
    actionBarDiv.appendChild(heartDiv);


    //Load inactive heart image
    var heartOff = document.createElement('img');
    heartOff.src = "Assets/heartOff.svg";
    heartDiv.appendChild(heartOff);
    heartOff.setAttribute('onclick', "heartChange(this)");
    heartOff.setAttribute('class', 'heartOff');

    //append arrow div to the action bar div
    actionBarDiv.appendChild(arrowDiv);

    //load and append image for arrow
    var expandImg = document.createElement('img')
    expandImg.src = "Assets/expand.svg";
    expandImg.setAttribute('onclick', "invertArrow(this)");
    expandImg.setAttribute('class', 'commentArrow');
    expandImg.setAttribute('id', number);
    expandImg.style.transform = "scaleY(1)";
    arrowDiv.appendChild(expandImg);

    //Create div for comment section
    const commentSec = document.createElement('div');
    commentSec.setAttribute('class', 'comments');
    commentSec.setAttribute('id', number);
    const horizontalLine = document.createElement('hr');
    commentSec.appendChild(horizontalLine);
    horizontalLine.setAttribute('class', 'line');

    //Create button to add a comment.
    const commentButtonDiv = document.createElement('div');
    commentButtonDiv.setAttribute('class', 'commentButton');
    commentButtonDiv.setAttribute('onclick', 'writeComment(this)');
    
    //Create span for "+" inside button
    const commentButtonSpan = document.createElement('span');
    commentButtonSpan.setAttribute('class', 'innerCommentButton');
    const commentButtonPlus = document.createTextNode("+");
    commentButtonSpan.appendChild(commentButtonPlus);

    commentSec.appendChild(commentButtonDiv);
    commentButtonDiv.appendChild(commentButtonSpan);

    //Create div housing avatar image and field for authoring a comment
    const commentWriterDiv = document.createElement('div'); 
    commentWriterDiv.setAttribute('class', 'commentWriterDiv');
    commentSec.insertBefore(commentWriterDiv, commentButtonDiv);

    //Add an avatar logo to the comment authoring div
    const avatarImage = document.createElement('img');
    avatarImage.src = "Assets/avatar-blue.svg";
    avatarImage.setAttribute('class', 'avatarImage');

    //Create a field wherein you can author a comment
    commentWriter = document.createElement('textarea');
    commentWriter.setAttribute('class', 'commentTextArea');
    commentWriter.setAttribute('placeholder', 'Skriv et svar');
    commentWriter.setAttribute('blur', "resetMargin()");

    //Make the field auto expand on text input
    var limitTextArea = 100;
    commentWriter.oninput = function() {
        console.log(commentWriter.scrollHeight)
        commentWriter.style.height = "";
        commentWriter.style.height = Math.min(commentWriter.scrollHeight, limitTextArea) + "px";
        
        //Makes sure margin under textfield also increases so the entire comment section expands to make room for more text.
        if (commentWriter.scrollHeight >= 36) {
            commentWriterDiv.style.marginBottom = "";
            commentWriterDiv.style.marginBottom = Math.min(commentWriter.scrollHeight, limitTextArea) - 36 + "px";
        }
    }

    //Add avatar image into comment authoring div
    commentWriterDiv.appendChild(avatarImage);

    //Add text field into comment authoring div
    commentWriterDiv.appendChild(commentWriter);

    //Append comment section div after taskbar;
    newDiv.appendChild(commentSec);

    //Prepends post div before all other posts
    //document.section.prepend(newDiv);
    var section = document.body.querySelectorAll('section');
    section[1].prepend(newDiv);

    document.getElementById("blur").style.display = "none"
    document.getElementById("formDiv").style.display = "none"

    //Reset all input fields value to an empty string (empty)
    postTitleform.value = '';
    postChapterform.value = '';
    postPageform.value = '';
    postAssignmentform.value= '';
    postDescriptionform.value = '';

    number++;
    console.log(newDiv.id);
};

function makePost(){
    document.getElementById("blur").style.display = "block"
    document.getElementById("formDiv").style.display = "flex"
}

function closeForm(){
    document.getElementById("blur").style.display = "none"
    document.getElementById("formDiv").style.display = "none"

    let postTitleform = document.getElementById('postTitleForm');
    postTitleform.value = '';

    let postChapterform = document.getElementById('chapterNr');
    postChapterform.value = '';

    let postPageform = document.getElementById('pageNr');
    postPageform.value = '';

    let postAssignmentform = document.getElementById('assignmentNr');
    postAssignmentform.value = '';

    let postDescriptionform = document.getElementById('descriptionForm');
    postDescriptionform.value = '';

}

function bellChange(elem){
    
    //Finds last stirng after a "/" in the elem
    let imgName = elem.src.split("/").pop();
    console.log(imgName);


    if (imgName == "bellOff.svg"){
        elem.setAttribute('src', "Assets/bellOn.svg");
    } else {
        elem.setAttribute('src', "Assets/bellOff.svg");
    }
}


function heartChange(elem){
        //Finds last stirng after a "/" in the elem
        let imgName = elem.src.split("/").pop();
        console.log(imgName);
    
    
        if (imgName == "heartOff.svg"){
            elem.setAttribute('src', "Assets/heartOn.svg");
        } else {
            elem.setAttribute('src', "Assets/heartOff.svg");
        }
}

//Function for opening comment section when clicking on the inverted Arrow
function invertArrow(elem){
    //Get's the style of the arrow and inverts it
    var style = window.getComputedStyle(elem).getPropertyValue("transform");
    if (style == "matrix(1, 0, 0, 1, 0, 0)"){
        elem.style.transform = "scaleY(-1)";
        
    } else {
        elem.style.transform = "scaleY(1)";
    }

    //Opens the appropriate comment section by first getting parrent post, finding div with class = comments
    //and then displaying it.
    var postDiv = elem.parentElement.parentElement.parentElement;

    var commentsDiv = postDiv.querySelector('.comments')

    var commentsDisplayStyle = window.getComputedStyle(commentsDiv).getPropertyValue("display");
    console.log(commentsDisplayStyle);

    if (commentsDisplayStyle == "none"){
        commentsDiv.style.display = "flex";
    } else {
        commentsDiv.style.display = "none";
    }
}

 //Resets the margins when focus is removes from the field
 function resetMargin() {
    commentWriterDiv.style.marginBottom = "0";
    console.log("focus is out");
}


function writeComment(elem){
    var CommentsDiv = elem.parentElement;
    var commentWriterDiv = CommentsDiv.querySelector('.commentWriterDiv');

    //Div housing the new thread under the post
    const newThread = document.createElement('div');
    newThread.setAttribute('class', 'commentThread');
    CommentsDiv.insertBefore(newThread, commentWriterDiv);

    
    
    //Div housing the first avatar comment pair
    const commentCase = document.createElement('div');
    commentCase.setAttribute('class', 'commentCase')
    newThread.appendChild(commentCase);

    //Div housing avatar next to written comment
    const avatarDiv = document.createElement('div');
    commentCase.appendChild(avatarDiv);
    avatarDiv.setAttribute('class', 'commentAvatar');
    const avatarImage = document.createElement('img');
    avatarImage.src = "Assets/avatar-blue.svg";
    avatarDiv.appendChild(avatarImage);
    

    //div housing text next to the avatar image
    const theCommentDiv = document.createElement('div');
    theCommentDiv.setAttribute('class', 'theCommentDiv');
    commentCase.appendChild(theCommentDiv);

    var commentTextArea = commentWriterDiv.querySelector('.commentTextArea');
    var theComment = document.createTextNode(commentTextArea.value);
    theCommentDiv.appendChild(theComment);
    commentTextArea.value = '';
    console.log(theComment)
}



//Test for script being loaded
console.log("The script is loaded");
